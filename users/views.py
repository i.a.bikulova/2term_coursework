from rest_framework import generics
from .serializers import UserSerializer


class RegisterUser(generics.CreateAPIView):
    serializer_class = UserSerializer
