from django.contrib.auth import get_user_model
from rest_framework import serializers

Users = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    id = serializers.CharField(read_only=True)

    class Meta:
        model = Users
        fields = ('id', 'username', 'password')

    def create(self, validated_data):
        username = validated_data.get('username')
        password = validated_data.get('password')

        user = Users.objects.create_user(username=username,
                                         password=password)

        return user
