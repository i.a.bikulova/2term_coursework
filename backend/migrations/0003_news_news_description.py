# Generated by Django 4.0.6 on 2022-07-07 10:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0002_news_news_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='news_description',
            field=models.CharField(default='', max_length=500),
        ),
    ]
