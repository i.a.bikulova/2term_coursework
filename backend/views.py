import uuid
from typing import Any

from django.core.files.storage import FileSystemStorage
from rest_framework import generics, mixins
from rest_framework.permissions import IsAuthenticated
from django.core.files import File

from .serializers import NewsSerializer, CommentSerializer, CarsSellingSerializer
from .models import News, NewsImagesRelation, Images, CarsImagesRelation, CarsSelling


class GetSerializerUserMixin(generics.GenericAPIView):
    def get_serializer(self, *args, **kwargs):
        kwargs['user'] = self.request.user
        return self.serializer_class(*args, **kwargs)


class PushFileUserView(generics.CreateAPIView, GetSerializerUserMixin):
    relation_class: Any
    field: str

    def get_object(self):
        pass

    def post(self, request, *args, **kwargs):
        response = self.create(request, *args, **kwargs)
        if len(request.FILES):
            fs = FileSystemStorage()
            for key in request.FILES.keys():
                file: File = request.FILES.get(key)
                file_uuid = uuid.uuid4()
                filename = f'{file_uuid}.{file.name.split(".")[-1]}'
                file.name = filename
                image_obj = Images.objects.create(image=file)

                query = {
                    self.field: self.get_object(),
                    'image': image_obj
                }
                self.relation_class.objects.create(**query)

        return response


class ListNewsView(generics.ListAPIView):
    serializer_class = NewsSerializer

    def get_queryset(self):
        return News.objects.all()


class ListCarsView(generics.ListAPIView):
    serializer_class = CarsSellingSerializer

    def get_queryset(self):
        return CarsSelling.objects.all()


class CreateNewsView(PushFileUserView):
    serializer_class = NewsSerializer
    permission_classes = (IsAuthenticated,)
    relation_class = NewsImagesRelation
    field = 'news'

    def get_object(self):
        return News.objects.all().last()


class CreateCommentView(generics.CreateAPIView, GetSerializerUserMixin):
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated, )


class CreateCarSelling(PushFileUserView):
    serializer_class = CarsSellingSerializer
    permission_classes = (IsAuthenticated, )
    relation_class = CarsImagesRelation
    field = 'car'

    def get_object(self):
        return CarsSelling.objects.all().last()
