from rest_framework import serializers
from .models import NewsImagesRelation, News, Comments, CarsSelling, CarsImagesRelation


class UserMixin(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        if 'user' in kwargs.keys():
            self.user = kwargs['user']
            del kwargs['user']
        else:
            self.user = None

        super().__init__(*args, **kwargs)


class NewsSerializer(UserMixin):
    news_text = serializers.CharField()
    news_title = serializers.CharField()
    news_description = serializers.CharField()
    pub_date = serializers.DateTimeField(source='news_pub_date', read_only=True)
    images = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()

    def get_images(self, obj):
        return [x.image.image.url for x in NewsImagesRelation.objects.filter(news_id=obj.id)]

    def get_comments(self, obj):
        return [{"username": x.username, 'text': x.comment_text} for x in Comments.objects.filter(news_id=obj.id)]

    def create(self, validated_data):
        if self.user:
            news_text = validated_data.get('news_text')
            news_title = validated_data.get('news_title')
            news_desc = validated_data.get('news_description')
            news = News(user=self.user, news_text=news_text, news_title=news_title, news_description=news_desc)
            news.save()
            return news


class CommentSerializer(UserMixin):
    news_id = serializers.CharField()
    comment_text = serializers.CharField(max_length=200)

    def create(self, validated_data):
        if self.user:
            text = validated_data.get('comment_text')
            news = News.objects.get(pk=validated_data.get('news_id'))

            return Comments.objects.create(user=self.user, news=news, comment_text=text)


class CarsSellingSerializer(UserMixin):
    news_text = serializers.CharField()
    pub_date = serializers.DateTimeField(source='news_pub_date', read_only=True)
    images = serializers.SerializerMethodField()

    def create(self, validated_data):
        if self.user:
            text = validated_data.get('news_text')
            return CarsSelling.objects.create(user=self.user, news_text=text)

    def get_images(self, obj):
        return [x.image.image.url for x in CarsImagesRelation.objects.filter(car_id=obj.id)]

