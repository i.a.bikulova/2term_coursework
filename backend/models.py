from django.db import models
# Import user model
from django.contrib.auth import get_user_model
from django.utils import timezone

Users = get_user_model()


class News(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    news_pub_date = models.DateTimeField(default=timezone.now)
    news_text = models.TextField()
    news_title = models.CharField(max_length=100, default="")
    news_description = models.CharField(max_length=500, default="")

class Images(models.Model):
    image = models.ImageField()


class CarsSelling(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    news_pub_date = models.DateTimeField(default=timezone.now)
    news_text = models.TextField()


class Comments(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    news = models.ForeignKey(News, on_delete=models.CASCADE)
    comment_text = models.CharField(max_length=200)


# Relations
class CarsImagesRelation(models.Model):
    car = models.ForeignKey(CarsSelling, on_delete=models.CASCADE)
    image = models.ForeignKey(Images, on_delete=models.DO_NOTHING)


class NewsImagesRelation(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE)
    image = models.ForeignKey(Images, on_delete=models.CASCADE)
