from django.urls import path
from .views import ListNewsView, CreateNewsView, CreateCommentView, CreateCarSelling, ListCarsView

urlpatterns = [
    path('list-news', ListNewsView.as_view()),
    path('list-cars', ListCarsView.as_view()),
    path('create-news', CreateNewsView.as_view()),
    path('create-comment', CreateCommentView.as_view()),
    path('create-car', CreateCarSelling.as_view()),
]
