class CONFIG {
    API_URL ="http://localhost:8000/api"
    TOKEN_URI = `${this.API_URL}/user/token`
    REGISTER_URI = `${this.API_URL}/user/register`
    CREATE_NEWS_URI = `${this.API_URL}/create-news`

    async sendPostRequest(url, data) {
        const r = await fetch(url, {
            method: "post",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        if (r.ok) {
            return await r.json()
        }
        const err = await r.json()
        throw new Error(`BAD REQUEST: ${JSON.stringify(err)}`)
    }
    async sendPostRequestMultipart(url, data, headers = {}) {
        const options =  {
            method: "post",
            body: data
        }
        
        if (Object.entries(headers).length) {
            options.headers = headers
        }
        
        const r = await fetch(url, options)
        if (r.ok) {
            return await r.json()
        }
        const err = await r.json()
        throw new Error(`BAD REQUEST: ${JSON.stringify(err)}`)
    }
}

export default new CONFIG;