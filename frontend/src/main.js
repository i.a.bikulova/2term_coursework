import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createStore } from 'vuex'
import StoreOptions from './store';

const app = createApp(App)
const store = createStore(StoreOptions)

app.use(router)
app.use(store)

app.mount('#app')
