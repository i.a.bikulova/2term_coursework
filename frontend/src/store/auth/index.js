import moment from 'moment';
import config from '@/config';
import Swal from 'sweetalert2';

export default {
    state: () => ({
        logged: localStorage.accessToken ? true : false,
        accessToken: localStorage.accessToken ? localStorage.accessToken : "",
        refreshToken: localStorage.refreshToken ? localStorage.refreshToken : "",
        tokenExp: localStorage.tokenExp ? localStorage.tokenExp : moment().valueOf(),
    }),

    getters: {
        logged: (state) => state.logged,
        accessToken: (state) => state.accessToken,
    },

    mutations: {
        changeAccessRefreshExp(state, settings) {
            state.logged = true;
            state.accessToken = settings.accessToken;
            state.refreshToken = settings.refreshToken;
            state.tokenExp = settings.tokenExp;

            localStorage.accessToken = state.accessToken;
            localStorage.refreshToken = state.refreshToken;
            localStorage.tokenExp = settings.tokenExp;
        },
        
        logout(state) {
            state.logged = false,
            state.accessToken = "",
            state.refreshToken = "",
            state.tokenExp = moment().valueOf()
            delete localStorage['accessToken']
            delete localStorage['refreshToken']
            delete localStorage['tokenExp']
        }
    },

    actions: {
        async auth({commit}, data) {
            try {
                const response = await config.sendPostRequest(config.TOKEN_URI, data)
                Swal.fire({
                    title: "Вы успешно вошли",
                    position: 'top-end',
                    icon: "success",
                    showConfirmButton: false,
                    timer: 1500
                })
                commit('changeAccessRefreshExp', {
                    accessToken: response.access, 
                    refreshToken: response.refresh, 
                    tokenExp: moment(moment().valueOf() + 5*60).valueOf()
                })
                
                return true
            }
            catch (err){
                console.log(err)
                Swal.fire({
                    title: "Неправильный логин или пароль",
                    text: "Неправильный логин или пароль. Пожалуйста, повторите попытку.",
                    icon: "error",
                })
                return false
            }

        }
    }
}