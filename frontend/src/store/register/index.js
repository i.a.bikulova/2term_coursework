import config from '@/config';
import Swal from 'sweetalert2';
export default {
    actions: {
        async registerUser({}, {username, password}) {
            try {
                const response = await config.sendPostRequest(config.REGISTER_URI, {
                    username,
                    password
                })
                Swal.fire({
                    title: "Пользователь успешно создан!",
                    text: "Сейчас вы перенаправитесь на страницу логина",
                    icon: 'success',
                })
                return true
            }
            catch {
                Swal.fire({
                    title: "Имя пользователя занято",
                    text: "Пожалуйста, выберите другое имя пользователя",
                    icon: "error",
                })
                return false
            }
        }
    }
}